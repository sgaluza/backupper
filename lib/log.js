var log = require('log4js');

log.configure('log.json');

module.exports = log.getLogger();