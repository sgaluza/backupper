var Promise = require('promise')
    , file7z = require('./tasks/file7zbackup')
    , path = require('path')
    , fs = require('fs')
    , config = require('./config')
    , moment = require('moment')
    , rimraf = require('rimraf')
    , util = require('util')
    , log = require('./lib/log')
    , ftpbackup = require('./tasks/ftpbackup');

var tempDir = config.get('path.temp')
    , dailyDir = config.get('path.daily')
    , cleanDir = function(path){
        if(fs.existsSync(path))
            rimraf.sync(path);
        require('mkdirp')(path);
    }
    , dirFiles = function(dir){
        return fs.readdirSync(dir).map(function(f){
            return path.join(dir, f)
        })
    };

var mom = moment();
var todayPath = path.join(dailyDir, mom.format('YYYYMMDD'));

cleanDir(todayPath);

Promise.resolve()
    // MySQL Backups
    .then(function() {
        if(config.has('mysql')) {
            cleanDir(tempDir);
            return require('./tasks/mysqldump')
                .then(function(){
                    var p = path.join(todayPath, 'mysql');
                    cleanDir(p);
                    log.info('Zipping MySQL DB Dumps');
                    return file7z(dirFiles(tempDir), p);
                });
        }else{
            return Promise.resolve();
        }
    })
    // MS SQL Backups
    .then(function() {
        if(config.has('mssql')) {
            cleanDir(tempDir);
            return require('./tasks/mssqlbackup')
                .then(function() {
                    var p = path.join(todayPath, 'mssql');
                    cleanDir(p);
                    log.info('Zipping MS SQL DB backups');
                    return file7z(dirFiles(tempDir), p)
                });

        } else{
          return Promise.resolve();
        }
    })
    // Simple File Backups
    .then(function(){
        if(config.has('files')) {
            var full = true; // full backup by default
            var lastFull = mom.date();
            var fullBackupDates = config.get('files.fullBackupDates')
                .map(function(v) { return parseInt(v); })
                .sort().reverse();

            // get latest full backup
            var found = false;
            for (var i = 0; i < fullBackupDates.length; i++) {
                if (lastFull > fullBackupDates[i]) {
                    lastFull = fullBackupDates[i];
                    full = false;
                    found = true;
                    break;
                } else if (lastFull === fullBackupDates[i]) {
                    found = true;
                    break;
                }
            }
            if (!found)
                lastFull = fullBackupDates.shift();

            var momFull = moment({year: mom.year(), month: mom.month(), day: lastFull});
            if (momFull > mom) {
                momFull = momFull.add(-1, 'months');
            }
            var fullPath = path.join(dailyDir, momFull.format('YYYYMMDD'), 'files');


            cleanDir(tempDir);
            var p = path.join(todayPath, 'files');
            var folders = config.get('files.folders');

            return folders.map(function (f) {
                return function () {
                    cleanDir(path.join(p, f.name));

                    log.info((full ? 'Full' : util.format('Differential (%s)', fullPath)) + ' Backup: ' + f.name)
                    if (f.archivePerFolder) {
                        return file7z(dirFiles(f.path), path.join(p, f.name), full ? false : path.join(fullPath, f.name));
                    } else {
                        return file7z(f.path, path.join(p, f.name), full ? false : path.join(fullPath, f.name));
                    }
                }
            })
                .reduce(function (cur, next) {
                    return cur.then(next);
                }, Promise.resolve());
        }
        else{
            return Promise.resolve();
        }
    })
    .then(function(){
        if(config.has('ftp')) {
            return ftpbackup(dailyDir);
        }
        else{
            return Promise.resolve();
        }

    })
    .then(function(){
        cleanDir(tempDir);
        log.info('Deleting old backups...');
        return require('./tasks/deleteold');
    })
    .then(function(){
        log.info('DONE');
    })
    .catch(function(err){
        log.error(err);
    });
