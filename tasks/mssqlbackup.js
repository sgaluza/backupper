var Promise = require('promise')
    , config = require('../config')
    , sql = require('mssql')
    , util = require('util')
    , path = require('path')
    , log = require('../lib/log')
    ;

module.exports = new Promise(function(resolve, reject){
    log.info('MSSQL Backup Started');
    var connection = new sql.Connection({
        user: config.get('mssql.user'),
        server: config.get('mssql.host'),
        password: config.get('mssql.pass'),
        connectionTimeout: 4*60*60*1000,
        requestTimeout: 4*60*60*1000
    });

    connection.connect()
        .then(function(){
            var request = new sql.Request(connection);
            return request.query('SELECT name FROM master.dbo.sysdatabases');
        })
        .then(function(dbs){
            var t = dbs.filter(function(db){
                if(!config.has('mssql.exclude')) return true;
                
                return config.get('mssql.exclude').indexOf(db.name) == -1;
            }).map(function(db){
                return function(){
                    dbname = db.name;
                    var cmd = util.format("BACKUP DATABASE [%s] TO DISK='%s'", dbname, path.join(config.get('path.temp'), dbname + '.bak'));
                    log.info(cmd);
                    var request = new sql.Request(connection);
                    return request.query(cmd);
                };
            })
                .reduce(function(cur, next){
                    return cur.then(next);
                }, Promise.resolve()).catch(function(err){
                    return reject(err);
                });
            return t;
        })
        .then(function(){
            log.info('MSSQL Backup Completed');
            resolve();
        })
        .catch(function (err){
            log.error(util.inspect(err));
            reject(err);
        });
});
