var Promise = require('promise')
    , config = require('../config')
    , sql = require('mssql')
    , spawn = require('child-process-promise').spawn
    , util = require('util')
    , path = require('path')
    , log = require('../lib/log')
    , fs = require('fs')
    ;

module.exports = new Promise(function(resolve, reject) {
    log.info('MS SQL Script Restore Started');
    var dbs = fs.readdirSync(config.get('path.temp'));

    dbs.map(function(db){
        return function(){
            db = path.basename(db, '.sql');
            log.info('Restoring database: ' + db);
            return spawn(config.get('mssql.pathToSqlCmd'),[
                "-S", config.get('mssql.host'),
                "-U", config.get('mssql.user'),
                "-P", config.get('mssql.pass'),
                "-d", db,
                "-i", path.join(config.get('path.temp'), db + '.sql')
            ])
                .progress(function (childProcess) {
                    log.debug('[spawn] childProcess.pid: ', childProcess.pid);
                    childProcess.stdout.on('data', function (data) {
                        log.debug('[spawn] stdout: ', data.toString());
                    });
                    childProcess.stderr.on('data', function (data) {
                        log.warn('[spawn] stderr: ', data.toString());
                    });
                });
        }
    })


}



