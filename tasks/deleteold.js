var log = '../lib/log'
    , fs = require('fs')
    , config = require('../config')
    , Promise = require('promise')
    , path = require('path')
    , rimraf = require('rimraf')
    ;

module.export = new Promise(function(resolve, reject){
    var pathDaily = config.get('path.daily');
    
    var folders = fs.readdirSync(pathDaily).filter(function(f){
        return fs.lstatSync(f).isDirectory();
    }).sort();

    var maxBackups = config.get('settings.maxBackups');
    for(var i=0;i<folders.length-maxBackups;i++){
        rimraf.sync(path.join(pathDaily, folders[i]));
    }
    resolve();
});
