var Promise = require('promise')
    , config = require('../config')
    , util = require('util')
    , path = require('path')
    , fs = require('fs')
    , spawn = require('child-process-promise').exec
    , log = require('../lib/log')
    ;

var pack = function (source, dest, fullPath) {
    var fileName = path.basename(source);
    if (fileName === '')
        fileName = path.dirname(source);
    var tempDest = path.join(config.get('path.temp'), fileName + '.7z');
    dest = path.join(dest, fileName + '.7z');
    
    if (fullPath) {
        fullPath = path.join(fullPath, fileName + '.7z');
        if (!fs.existsSync(fullPath)) {
            log.warn("Can't find previous backups, switching to full backup mode for " + source);
            fullPath = false;
        }
    }
    
    /*var cmd = fullPath
        ? util.format('"%s" u "%s" -u- -up0q3x2z0!"%s" "%s" -y', config.get('path.to7z'),
            fullPath, tempDest, source)
        : util.format('"%s" a "%s" "%s" -y', config.get('path.to7z'), tempDest, source);*/

    //log.info(cmd);

/*    var params = fullPath
        ? ["u", '"' + fullPath + '"', '"' + source + '"', "-u-", util.format('-up0q3r2x2y2z0w2!"%s"', tempDest)]
        : ["a", tempDest, source, "-y"];*/


    var runexe = fullPath
        ? '"' + config.get('path.to7z') + '" u "' + fullPath + '" "' + source + '" -u- -up0q3r2x2y2z0w2!"' + tempDest + '" -y'
        : '"' + config.get('path.to7z') + '" a "' + tempDest + '" "' + source + '" -y'
    var password = config.get("files.password");
    if(password){
        runexe += ' -p' + password;
    }



    return spawn(runexe, {maxBuffer: 1024*1024*512})
    .progress(function (childProcess) {
        log.debug('[spawn] childProcess.pid: ', childProcess.pid);
        childProcess.stdout.on('data', function (data) {
            log.debug('[spawn] stdout: ', data.toString());
        });
        childProcess.stderr.on('data', function (data) {
            log.warn('[spawn] stderr: ', data.toString());
        });
    })
    .then(function () {
        return new Promise(function(resolve, reject){
            var p = fs.createReadStream(tempDest).pipe(fs.createWriteStream(dest));
            p.on('close', function(){
                log.debug('copied: ' + dest);
                resolve();
            })
        });
    });
/*    .fail(function (err) {
        log.error('[spawn] stderr: ', util.inspect(err));
    })*/
};

module.exports = function (source, dest, fullPath) {
    if (Array.isArray(source)) {
        return source.map(function (s) {
            return function () {
                return pack(s, dest, fullPath);
            }
        }).reduce(function (cur, next) {
            return cur.then(next);
        }, Promise.resolve());
    }
    else {
        return pack(source, dest, fullPath)
    }
};