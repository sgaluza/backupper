var Promise = require('promise')
    , config = require('../config')
    , sql = require('mssql')
    , spawn = require('child-process-promise').spawn
    , util = require('util')
    , path = require('path')
    , log = require('../lib/log')
    ;

module.exports = new Promise(function(resolve, reject) {
    log.info('MS SQL Script Backup Started');

    var connection = new sql.Connection({
        user: config.get('mssql.user'),
        server: config.get('mssql.host'),
        password: config.get('mssql.pass'),
        connectionTimeout: 4*60*60*1000,
        requestTimeout: 4*60*60*1000
    });

    return connection.connect()
        .then(function(){
            var request = new sql.Request(connection);
            return request.query('SELECT name FROM master.dbo.sysdatabases');
        })
        .then(function(dbs) {
            var t = dbs.filter(function(db){
                return config.get('mssql.exclude').indexOf(db.name) == -1;
            }).map(function(db){
                return function() {
                    var dbname = db.name;
                    log.info('Scripting database: ' + dbname);
                    return spawn(config.get('mssql.pathToScripter'), [
                        'generate',
                        util.format('-s=%s', config.get('mssql.host')),
                        util.format('-db=%s', dbname),
                        util.format('-u=%s', config.get('mssql.user')),
                        util.format('-p=%s', config.get('mssql.pass')),
                        util.format('-f=%s', path.join(config.get('path.temp'), dbname + '.sql'))
                    ])
                        .progress(function (childProcess) {
                            log.debug('[spawn] childProcess.pid: ', childProcess.pid);
                            childProcess.stdout.on('data', function (data) {
                                log.debug('[spawn] stdout: ', data.toString());
                            });
                            childProcess.stderr.on('data', function (data) {
                                log.warn('[spawn] stderr: ', data.toString());
                            });
                        });
                }
            })
            .reduce(function(cur, next){
                return cur.then(next);
            }, Promise.resolve());
            return t;
        })
        .then(function(){
            log.info('MS SQL Script Backup Completed');
            resolve();
        })
        .catch(function(err){
            reject(err);
        });
});