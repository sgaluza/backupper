var Promise = require('promise')
    , ftpsync = require('ftpsync')
    , config = require('../config')
    , path = require('path')
    , log = require('../lib/log')
    , util = require('util');

module.exports = function(source){
    ftpsync.settings.host = config.get("ftp.host");
    ftpsync.settings.user = config.get("ftp.user");
    ftpsync.settings.pass = config.get("ftp.pass");
    ftpsync.settings.local = source;

    return new Promise(function(resolve, reject) {
        ftpsync.run(function(){
            resolve();
        })
    });
}


