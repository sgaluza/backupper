var Promise = require('promise')
    , config = require('../config')
    , mysql = require('promise-mysql')
    , child = require('child_process')
    , path = require('path')
    , fs = require('fs')
    , log = require('../lib/log');

module.exports = new Promise(function(resolve, reject){
    var connection;
    log.info('MySQL Dump Started');

    mysql.createConnection({
        host: config.get('mysql.host'),
        user: config.get('mysql.user'),
        password: config.get('mysql.pass')
    })
        .then(function(conn){
            connection = conn;
            return conn.query('SHOW DATABASES');
        })
        .then(function(values){
            connection.end();
            return values.filter(function(db){
                var dbname = db['Database'];
                return  dbname != 'performance_schema'
                    && dbname != 'mysql'
                    && dbname != 'information_schema';
            }).map(function(db){
                var dbname = db['Database'];
                return function(){
                    return new Promise(function(resolve, reject){
                        var mysqldumpexe = config.has('mysql.mysqldump')
                            ? config.get('mysql.mysqldump')
                            : 'mysqldump';
                        var args = config.has('mysql.loginPath')
                            ?   ["--login-path="+config.get('mysql.loginPath'), "--hex-blob", dbname]
                            :   ["--hex-blob", "-h", config.get('mysql.host'), "-u"+config.get('mysql.user'), "-p"+config.get('mysql.pass'), dbname];
                        log.info('MySQL Dump database: ' + dbname);
                        var proc = child.spawn(mysqldumpexe, args);
                        var file = fs.createWriteStream(path.join(config.get('path.temp'), dbname+'.sql'));
                        proc.stdout.pipe(file);
                        proc.stderr.on('data', function(output){
                            reject(new Error(output));
                        });
                        proc.on('close',function(code){
                            resolve();
                        });

                    });
                };
            }).reduce(function(cur, next){
                return cur.then(next);
            }, Promise.resolve());
        })
        .then(function(){
            log.info('MySQL Dump Completed');
            resolve();
        })
        .catch(function(err){
            reject(err);
        });
});
