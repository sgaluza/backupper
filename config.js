var convict = require('convict');

var conf = convict({
    path:{
        daily: "c:\\backups\\daily",
        temp: "d:\\temp",
        to7z: "c:\\Program Files\\7-Zip\\7za.exe"
    },
    mysql: {
        host: "localhost",
        user: "root",
        pass: "pass",
        loginPath: "local" // this string is for mysqldump:
                           // mysql_config_editor set --login-path=local --host=localhost --user=username --password,
    },
    mssql: {
        host: "127.0.0.1",
        user: "sa",
        pass: "pass",
        pathToSqlPubWiz: "c:\\Program Files (x86)\\Microsoft SQL Server\\90\\Tools\\Publishing\\1.4\\sqlpubwiz.exe"
    },
    files: {
        password: "123",
        folders:[
            { name:'websites', path: "d:\\websites", archivePerFolder: true },
        ],
        fullBackupDates: [1, 10, 20] //backup days of months
    },
    ftp:{
        host: "somehost",
        user: "user",
        pass: "pass"
    },
    settings:{
        maxBackups: 2
    }
});

module.exports = conf;